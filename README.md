# 更优方案
AE直接导出雪碧图
[CSS-Sprite-Exporter](https://gitee.com/hubzyy/CSS-Sprite-Exporter)

原本方案是将序列帧合并成雪碧图，具有一定局限性，最新方案相对更优～


# SequenceAnimationMerge
序列针合成CSS动画交付前端工具


```
//不安装也行
！！先安装node环境

#全局安装 http-server
npm i http-server -g

#运行项目
http-server ./

```
1. 输入对应的文件夹和图片名称
1. 输入开始序号-结束序号
1. 设置CSS动画秒速
1. 复制代码
1. 右键保存图片



![](img/demo.png)
![](img/demo2.png)